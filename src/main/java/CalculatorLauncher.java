public class CalculatorLauncher {
    public static void main(String[] args) {
        System.out.println(addition(3, 4));
        System.out.println(subtraction(3, 4));
        System.out.println(multiplication(3, 4));
        System.out.println(division(3, 0));
        System.out.println(division(3, 1));
    }

    public static int addition(int a, int b) {
        return a + b;
    }

    public static int subtraction(int a, int b) {
        return a - b;
    }

    public static int multiplication(int a, int b){
        return a*b;
    }

    // gdy b = 0 program zwracał wartosc "Infinity", stąd brak obsługi zero division error
    public static double division(int a, int b){
        if(b == 0){
            System.out.println("You can't divide by 0");
            return 0;
        }else{
            return (double)a/(double)b;
        }

    }
}
